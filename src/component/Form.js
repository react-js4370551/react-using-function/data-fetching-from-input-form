import React, { useState } from 'react'

export default function Form() {
    const [product, setProduct] = useState({
        name : '',
        email : '',
        state : '',
        stock : false
    })
    
    function handleName(e){
        setProduct({
            name : e.target.value,
            email : product.email,
            state : product.state,
            stock : product.stock
        })
    }

    function handleEmail(e){
        setProduct({
            name : product.name,
            email : e.target.value,
            state : product.state,
            stock : product.stock
        })
    }
    function handleState(e){
        setProduct({
            name : product.name,
            email : product.email,
            state : e.target.value,
            stock : product.stock
        })
    }
    function handleStock(e){
        setProduct({
            name : product.name,
            email : product.email,
            state : product.state,
            stock : e.target.checked
        })
    }

    return (
        <div className='container'>
            <div className='row'>
                <div className='col-md-5'>
                    <h3>Reading Form Input field : </h3>
                    <p>{JSON.stringify(product)}</p>
                    <dl>
                        <dt>Name</dt>
                        <dp><input type='text' onChange={handleName} className='form-control' /></dp>
                        <dt>Email</dt>
                        <dp><input type='text'  onChange={handleEmail} className='form-control' /></dp>
                        <dt>State</dt>
                        <dp>
                            <select class="form-select"  onChange={handleState} aria-label="Default select example">
                                <option selected>Select Menu</option>
                                <option value="UP">UP</option>
                                <option value="MP">MP</option>
                            </select>
                        </dp>
                        <dt>Stock</dt>
                        <dp className='form-switch'>
                          <input type='checkbox'  onChange={handleStock} className='form-check-input'/> available
                        </dp>
                    </dl>
                </div>
            </div>

        </div>
    )
}
