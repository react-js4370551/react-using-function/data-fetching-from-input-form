import React, { useState } from 'react'

export default function OptimizedFormInputFieldReading() {
    const [product, setProduct] = useState({
        name: '',
        email: '',
        state: '',
        stock: false
    })

    function handleInputField(e) {
        if (e.target.type == 'checkbox') {
            setProduct({
                //copy product
                ...product,
                //update copied product
                [e.target.name]: e.target.checked
            })
        } else {
            setProduct({
                ...product,
                [e.target.name]: e.target.value
            })
        }

    }
    return (
        <div className='container'>
            <div className='row'>
                <div className='col-md-5'>
                    <h3>Optimized way to Reading Form Input field : </h3>
                    <p>{JSON.stringify(product)}</p>
                    <dl>
                        <dt>Name</dt>
                        <dp><input type='text' name="name" onChange={handleInputField} className='form-control' /></dp>
                        <dt>Email</dt>
                        <dp><input type='text' name="email" onChange={handleInputField} className='form-control' /></dp>
                        <dt>State</dt>
                        <dp>
                            <select className="form-select" name="state" onChange={handleInputField}>
                                <option selected>Select Menu</option>
                                <option value="UP">UP</option>
                                <option value="MP">MP</option>
                            </select>
                        </dp>
                        <dt>Stock</dt>
                        <dp className='form-switch'>
                            <input type='checkbox' name="stock" onChange={handleInputField} className='form-check-input' /> available
                        </dp>
                    </dl>
                </div>
            </div>

        </div>
    )
}
