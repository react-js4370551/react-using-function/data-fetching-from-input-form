import logo from './logo.svg';
import './App.css';
import Form from './component/Form';
import OptimizedFormInputFieldReading from './component/OptimizedFormFieldReading';

function App() {
  return (
    <div>
      <Form />
      <hr/>
      <OptimizedFormInputFieldReading />
    </div>
  );
}

export default App;
